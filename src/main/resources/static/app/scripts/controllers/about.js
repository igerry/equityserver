'use strict';

/**
 * @ngdoc function
 * @name ubstestApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ubstestApp
 */
angular.module('ubstestApp')
  .controller('', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });



