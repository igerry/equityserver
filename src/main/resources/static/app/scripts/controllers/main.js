'use strict';

/**
 * @ngdoc function
 * @name ubstestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ubstestApp
 */
angular.module('ubstestApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
