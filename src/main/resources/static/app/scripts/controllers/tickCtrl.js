'use strict';

var tickDataAppModule = angular.module('tickDataApp', ['ui.router','ui.grid']);



tickDataAppModule.config(
    [          '$stateProvider', '$urlRouterProvider',
        function ($stateProvider,   $urlRouterProvider) {

      /////////////////////////////
      // Redirects and Otherwise //
      /////////////////////////////

      // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
      $urlRouterProvider
        // If the url is ever invalid, e.g. '/asdf', then redirect to '/filters' 
        .otherwise('/filters');

        
      //////////////////////////
      // State Configurations //
      //////////////////////////   
       $stateProvider
         .state('filters', {
            url: "/filters",
           templateUrl: './partials/filters.html',
           controller: function($scope){
               $scope.title = 'filters';
               $scope.isConnected='false';
               $scope.frequency=5 ;
           }, 
           data: {
              appState: "filter"
            }
         })
        .state('filters.streaming', {
            resolve:{
                eqDataServiceDefn: 'eqDataService',
                localDataStore: 'localDataStoreService'
            },
            url: "/streaming",
            templateUrl: './partials/stock.html',
            controller: 'dataCtrl',
            data: {
              appState: "stream",
              lastTick: -1
            }
         })
        .state('filters.inrange', {
            resolve:{
                eqDataServiceDefn: 'eqDataService',
                localDataStore: 'localDataStoreService'
            },
            url: "/inrange",
            templateUrl: './partials/stock.html',
            controller: 'dataCtrl',
            data: {
              appState: "inrange",
              lastTick: -1
            }
         })
     }   
  ]
);




tickDataAppModule.controller('dataCtrl', ['$scope', '$log', '$http', 'chartService',
                             'eqDataServiceDefn','tableService','localDataStore','$interval','$state','uiGridConstants', 
                             function ($scope,  $log,   $http,   chartService, eqDataServiceDefn, tableView ,localDataStoreService, $interval, $state, uiGridConstants) {
    
    $log.info("initializing Controller [appState=" + $state.current.data.appState +"]" );
    $scope.frequency = 10; 
     
    $scope.charts = [{id : 'chart#1', divid : 'chart1'}, {id : 'chart#2', divid : 'chart2'}];
    eqDataServiceDefn.initService();                                             
    localDataStoreService.initService();                                        
                                              
    $scope.title = $state.current.data.appState;                                      
    $scope.lastTickId = $state.current.data.lastTick;
    $scope.isConnected = function(){
        return eqDataServiceDefn.isConnected();
    };
            
    // Define the grid attributes
    $scope.columns = [{ field: 'Id', sort: {direction: uiGridConstants.DESC, priority: 1} },{field:'Stock'},{ field: 'Time' }, { field: 'Price' }];
    $scope.gridOptions = {
      enableSorting: true,
      columnDefs: $scope.columns,
        onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
      }
    };
    
    
   
    $scope.updateDataFromSource = function(interval) {
    	eqDataServiceDefn.getStocks()  
            .then(function(stockMeta) {
                   function filterById(unfiltered, id){
                	   var filtered = [];
                	   for (var i = 0; i < unfiltered.length; i += 1) {
              	           var stock = unfiltered[i].stock;  
              	           if (stock == id){
              	    	      filtered.push(unfiltered[i]);
              	           }
              	        }
                	   return filtered;
                   }
                   var newData = localDataStoreService.take($scope.lastTickId);
                   chartService.updateChart($scope, $scope.charts[0], stockMeta.data[0], filterById(newData,  stockMeta.data[0] ));  
                   chartService.updateChart($scope, $scope.charts[1], stockMeta.data[1], filterById(newData,  stockMeta.data[1] )); 
                   $scope.gridOptions.data = tableView.update(newData);
                   $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.ALL);
                   var tickPre = $scope.lastTickId; 
                   $scope.lastTickId = localDataStoreService.lastTickInTickData(newData);
                   var tickPost = $scope.lastTickId;
                   console.log("lastTickId updated from " + tickPre + " to " + tickPost);
            }, function(error) {
                 console.log('Failure whilst getting data from server', error);
            });                             
    };
    

    $scope.updateFrequency = function(frequency) {
        console.log('Changing update freq', frequency);
        if (angular.isNumber(frequency) && frequency > 0 && frequency < 10000) {
        	
            //destroy current timer
            $interval.cancel($scope.updateDataTimer);
            //create new timer with new frequency
            $scope.updateDataTimer = $interval(
                function () {
                    $scope.updateDataTimer($scope.frequency);
                },
                frequency * 1000
            );
        }
    };

    //start data provider call update timer
    $scope.updateDataTimer = $interval(
        function() {
            $scope.updateDataFromSource($scope.frequency);
        },
        $scope.frequency * 1000
    );

    $scope.$on(
        "$destroy",
        function( event ) {
            $interval.cancel($scope.updateDataTimer);
        }
    );

    $scope.updateDataFromSource($scope.frequency);                                                
                                                     
}]);



// -----------------------------------------------------------------------------------------------------------
// model - equity data for chart and table
//
// -----------------------------------------------------------------------------------------------------------

tickDataAppModule.service(
                "localDataStoreService",
                function( $http, $q, $interval, $log, $state, $timeout ) {
                    // Return public API.
                    return({
                        initService: initService,
                        lastTickInTickData: lastTickInTickData,
                        update: update,
                        take: take
                    });

       
        function initService( $scope) {
           $log.info("Initialise data store");
           this.list = [];                             // Client side cache of tick data
           this.latestTickId = -1;                     // id of last tick returned in the most recent take function call
        };


        function lastTickInTickData(data) {
           return this.latestTickId;     
        };
        
       
           
        // Update the local model with new data             
        function update( data ) {
        	var MaxTicksRetained = 5000;
        	var transformed = function(jsonTickMsg){
        	    var item = JSON.parse(jsonTickMsg.body );
            	return item;
        	} 
            this.list.push(transformed(data)); 
            if (this.list.length > MaxTicksRetained ){   
              this.list.shift();    
            }
        };            

        function take(lastTickId) {
           var delta = [];
           for (var i=0; i < this.list.length; i=i+1){
               var tickArr = this.list[i];
               for (var j=0; j < tickArr.length; j=j+1){
                   if ((tickArr[j].id) > lastTickId){
            	       delta.push(tickArr[j]);
            	       this.latestTickId = tickArr[j].id;
                   }    
               }
               
           }
           return delta;
        }
        
    }           
);



// -----------------------------------------------------------------------------------------------------------
// backend server interface
// 
// -----------------------------------------------------------------------------------------------------------
tickDataAppModule.service(
                "eqDataService",
                ['$http', '$log', '$state','localDataStoreService', function($http,$log, $state,localDataStore) {
                    // Return public API.
                    return({
                        initService: initService,
                        getAll : getAll,
                        getStocks: getStocks,
                        connected : connected
                    });

       
 
        function initService( $scope) {  
            $log.info("initializing equity data service [appState=" + $state.current.data.appState +"]" );
     
  
            this.isConnected = false;
            var stompClient = null;
            var socket = null;
      
            function subscribeToTickUpdates() {
                
              var socketID = '/ticks';    // Assume javascript is sourced from same domain as the message publisher
              socket = new SockJS(socketID);
              stompClient = Stomp.over(socket); 
              stompClient.connect('','',function (frame){
            	 console.log("connected to socket "+socketID);
            	 this.isConnected = true;
                 stompClient.subscribe('/topic/tickdata', function(tickdata) {
                      onTopicUpdate(tickdata);
                 });
              });  
            }
            
            subscribeToTickUpdates();
            
        };
        
        function onTopicUpdate(msg) {
           console.log("Received topic update");
           localDataStore.update(msg);
        }
    
        function connected( $scope) {  
            return this.isConnected;
        }
        
        function getStocks(){
        	return $http.get('/stocks');   
        }
        
                    
        // Return all server data for all equities            
        function getAll( $scope) {
           $log.info("Equity data service - getAll [appState=" + $state.current.data.appState +"]" );
          
            
         function dummyData() {
             var vod = [];
             var ora = [];

             for (var i = 0; i < 100; i++) {     
                 var dval = new Date(99,5,24,11,33,30,i*3).getTime();
                 vod.push({symbol: 'VOD.L', x: dval, y: i % 10  });
                 ora.push({symbol: 'ORA.L',x: dval, y: i % 10  });
             }

             return [
               {
                area: false,
                values: vod,
                key: 'VOD.L',
                color: "#ff7f0e"
               },
               {
                area: false,
                values: vod,
                key: 'ORA.L',
                color: "#ff7f0e"
               }     
                 
             ];
          }
           var deferred = $q.defer();
           $timeout(function() {
                 deferred.resolve(dummyData());
           }, 10);
           return deferred.promise;
        };            
    }
])


   ///////////////////////////////////////////////////////////////////////////////////////////////////
   // View provider
   // Chart Service 
   // 
   ///////////////////////////////////////////////////////////////////////////////////////////////////

   tickDataAppModule.service(
                "chartService",
                function(localDataStoreService, $interval, $log ) {
                    // Return public API.
                    return({
                        updateChart: updateChart
                    });
          
                       
    function updateChart($scope, chartMeta, stockTickerId, filtered) {
        var chartdata = []; 
        var labels = [];
        $log.info("Rendering to " + chartMeta.divid)		    	
    
        // Transform the model data into a format recognized by the chart widget
        function transformModelData(filterById) {
        	var transformed = [];
      	    
      	    if ( filtered.length > 0){
      	        for (var i = 0; i < filtered.length; i += 1) {
      	           var p = filtered[i];  // Filter data to be for this chart's stock 
      	           var time = new Date(p.ticked.epochSecond * 1000); // Convert seconds to milliseconds as required by Date constructor
      	    	   transformed.push([(time.getTime() ) , p.price]);
      	        }
      	    }else{
      	    	transformed.push([(new Date()).getTime(),0]);
      	    }
      	    return transformed;
        };
        
       Highcharts.setOptions({
        global : {
            useUTC : false
        }
       });
        
       
       var chart = new Highcharts.StockChart ({
            chart : {
               
            renderTo: chartMeta.divid,     
//            events : {
//                load : function () {
//                    // set up the updating of the chart each second
//                    var series = this.series[0];
//                    setInterval(function () {
//                    	var delta = transformModelData();
//                    	delta.forEach(function(entry) {
//                    	    var time = new Date(entry[0]);
//                    	    series.addPoint([time, entry[1]],true,true );
//                    	});
//                    }, $scope.frequency * 1000);
//                }
//            }
        },
        rangeSelector: {
            buttons: [{
                count: 1,
                type: 'minute',
                text: '1M'
            }, {
                count: 5,
                type: 'minute',
                text: '5M'
            }, {
                type: 'all',
                text: 'All'
            }],
            inputEnabled: false,
            selected: 0
        },

        title : {
            text : stockTickerId
        },
        subtitle: {
            text: 'update freq ' + $scope.frequency + "sec"
        },   
        exporting: {
            enabled: false
        },

        series : [{
            name : chartMeta.key,
            data : (function () {            	    	  
            	  return transformModelData(stockTickerId);
                }())
               }]
        });
        
        
    };   // end of function  
  });  // end of service definition 

                    
                    
                    
   ///////////////////////////////////////////////////////////////////////////////////////////////////
   // Tick Data Table 
   // The table generation functions
   ///////////////////////////////////////////////////////////////////////////////////////////////////
                    
   tickDataAppModule.service(
                "tableService",
                ['$log', 'localDataStoreService', function($log,localDataStore) {
                    // Return public API.
                    return({
                        initTable: initTable,
                        update: update
                    });
                    
    function initTable() {
       $log.info("Initialising table " + tableId + " NOW");
    }
    
    
    function update(tickData) {
    	
    	 // Transform the model data into a format recognized by the ui-grid widget
        	var transformed = [];
      	    if ( tickData.length > 0){
      	        for (var i = 0; i < tickData.length; i += 1) {
      	           var p = tickData[i];
      	    	   transformed.push({"Id": p.id, "Stock":p.stock, "Time": new Date(p.ticked.epochSecond * 1000) , "Price" : p.price});
      	        }
      	    }
      	    return transformed;
     }
  }
]);