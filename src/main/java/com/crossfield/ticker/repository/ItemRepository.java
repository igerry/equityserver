package com.crossfield.ticker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crossfield.ticker.model.Item;

public interface ItemRepository extends JpaRepository<Item, Integer> {

}
