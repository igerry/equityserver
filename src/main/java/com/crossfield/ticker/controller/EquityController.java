package com.crossfield.ticker.controller;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crossfield.ticker.model.Item;
import com.crossfield.ticker.model.StockTick;

@RestController
public class EquityController {
  

  @RequestMapping("/stocks")
  public List<String> findStocks() {
      return Arrays.asList(new String[]{"ORA.L","VOD.L"});
  }
 

}