package com.crossfield.ticker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.crossfield.ticker.model.StockTick;

/**
 * Provide the real time tick data using STOMP. 
 *
 */
@Controller
public class TickUpdateController {
  
  private SimpMessagingTemplate template;
  
  @Autowired
  public TickUpdateController(SimpMessagingTemplate template) {
    this.template = template;
  }

  
  @MessageMapping("/ticks")
  public void handshake(Message<Object> message, @Payload StockTick chatMessage) throws Exception { 
    template.convertAndSend("/topic/tickdata", chatMessage);
  }

}