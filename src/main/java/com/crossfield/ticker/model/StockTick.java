package com.crossfield.ticker.model;

import java.time.Instant;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class StockTick {
  
  public static class Builder{
	  private final static AtomicLong id = new AtomicLong();
	  private String stock;
	  private Double price;
	  private Instant ticked;

	  
	  public Builder addStock(final String stock){
		  this.stock=stock;
		  return this;
	  }
	  public Builder addPrice(final Double price){
		  this.price=price;
		  return this;
	  }
	  public Builder addTicked(final Instant ticked){
		  this.ticked=ticked;
		  return this;
	  }
	  
	  public StockTick create(){
		  return new StockTick(id.getAndIncrement(), 
				                      stock,
				                      price,
				                      ticked);
	  }
  }
	
  private Long   id;
  private String stock;
  private Double price;
  private Instant ticked;
  
  private StockTick(){
	  
  }
  
  
  private StockTick(Long id,String stock, Double price, Instant ticked){
	  this.id=id;
	  this.stock=stock;
	  this.price=price;
	  this.ticked=ticked;
  }
 
  public Long getId() { return id; }
 
  public Double getPrice() { return price; }
  
  public Instant getTicked() { return ticked; }

  public String getStock() { return stock; }
  
  
  @Override
  public int hashCode(){
	  return new HashCodeBuilder().append(id).hashCode();
  }
  
  @Override
  public boolean equals(Object rhs){
	  if (rhs != null && rhs instanceof StockTick) {
          return new EqualsBuilder().append(this.id, ((StockTick) rhs).getId()).isEquals();
      } else {
          return false;
      }
  }
  
}
