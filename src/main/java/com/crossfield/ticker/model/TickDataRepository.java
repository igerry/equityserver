package com.crossfield.ticker.model;

import java.time.Instant;
import java.util.List;

public interface TickDataRepository {

	List<StockTick> getStockTicks(Instant start, Instant end);
}