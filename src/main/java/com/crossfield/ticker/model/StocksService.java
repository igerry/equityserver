package com.crossfield.ticker.model;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

@Service("stockTickProvider")
public class StocksService implements TickDataRepository {
  
  @Autowired
  private TickRepository repo;
 
  @Override
  public List<StockTick> getStockTicks(final Instant start, final Instant end) {
     return repo.findAllBetween(start, end);
  }
  
}
