package com.crossfield.ticker.model;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;

public class StockTickProvider {
	
  private final SimpMessagingTemplate template;
  private final TickDataRepository stocksService;
  private Instant lastUpdated;
  
  public StockTickProvider(SimpMessagingTemplate template, TickDataRepository stocksService) {
    this.template = template;
    this.stocksService = stocksService;
  }
  
  @Scheduled(fixedDelay = 2000)
  public void updateTick() {
	final Instant now = Instant.now();  
    template.convertAndSend("/topic/tickdata", stocksService.getStockTicks(lastUpdated, now));
    lastUpdated = now;
  }

}
