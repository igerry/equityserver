# Ticker
Front to back Equity ticker I wrote to 

* examine using STOMP between front-end and Spring/Java backend.
* Understand latency when deploying to PaaS such as Heroku

## Frameworks

### Front-end

#### AngularJS
AngularJS is a MVC based framework for web applications, written in JavaScript. 


### Back-end

#### Spring Boot
Provide the web wiring

#### Spring Data JPA
Spring Data JPA allows you to create repositories for your data without even having to write a lot of code. The only code you need is a simple interface that extends from another interface and then you're done.
With Spring Boot you can even leave the configuration behind for configuring Spring Data JPA, so now it's even easier.


## Installation
Installation is quite easy, first you will have to install some front-end dependencies using Bower:
```
bower install
```

Then you can run Maven to package the application:
```
mvn clean package
```

Now you can run the Java application quite easily:
```
cd target
java -jar ticker-1.0.0.jar
```